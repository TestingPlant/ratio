use anyhow::Result;
use chrono::prelude::*;
use colored::{ColoredString, Colorize};
use file_lock::{FileLock, FileOptions};
use image::io::Reader;
use image::DynamicImage;
use prost::Message;
use reqwest::header::HeaderValue;
use serde::{Deserialize, Serialize};
use std::io::Cursor;
use std::io::{Read, Seek, SeekFrom, Write};
use std::path::Path;

mod data {
    include!(concat!(env!("OUT_DIR"), "/data.rs"));
}

/// time - Time given by the API
fn parse_time(time: &str) -> NaiveDateTime {
    const NANOSECONDS_IN_SECOND: f64 = 1e+9_f64;
    let time: f64 = time.parse().unwrap();
    let seconds = time as i64;
    let nanoseconds = ((time - (seconds as f64)) * NANOSECONDS_IN_SECOND) as u32;
    NaiveDateTime::from_timestamp_opt(seconds, nanoseconds).unwrap()
}

/// time - Time given by the API
fn format_time(time: &str) -> ColoredString {
    format!("{}", parse_time(time).format(DATE_FORMAT)).blue()
}

fn make_token_cookie(token: &str) -> HeaderValue {
    HeaderValue::from_str(format!("session_DO_NOT_SHARE={}", token).as_str()).unwrap()
}

const DATE_FORMAT: &str = "%H:%M:%S on %b %-d, %Y";

fn main() {
    let error_text = "error".red();

    let config_home_path = std::env::var("XDG_CONFIG_HOME").ok().unwrap_or_else(|| {
        format!(
            "{}/.config",
            std::env::var("HOME").expect("HOME must be defined")
        )
    });
    let config_file_path = Path::new(config_home_path.as_str()).join("ratio");
    let mut config_file_lock = FileLock::lock(
        config_file_path,
        true,
        FileOptions::new().read(true).write(true).create(true),
    )
    .unwrap();

    let mut serialized_config = vec![];
    config_file_lock
        .file
        .read_to_end(&mut serialized_config)
        .unwrap();
    let mut config = data::Config::decode(serialized_config.as_slice()).unwrap();

    let http_client = reqwest::blocking::Client::default();

    let cmd = clap::Command::new("ratio")
        .bin_name("ratio")
        .subcommand_required(true)
        .subcommands([
            clap::command!("list-posts").arg(clap::arg!(<address> "address of the server")),
            clap::command!("create-post").args(&[
                clap::arg!(<address> "address of the server"),
                clap::arg!(<username> "username of the account to use to make the post"),
                clap::arg!(<title> "post title"),
                clap::arg!(<image_file> "image to upload"),
            ]),
            clap::command!("delete-post").args(&[
                clap::arg!(<address> "address of the server"),
                clap::arg!(<username> "username of the account to use to delete the post"),
                clap::arg!(<post> "id of the post to delete"),
            ]),
            clap::command!("create-comment").args(&[
                clap::arg!(<address> "address of the server"),
                clap::arg!(<username> "username of the account to use to comment with"),
                clap::arg!(<post> "id of the post to comment to"),
                clap::arg!(<text> "text to comment with"),
            ]),
            clap::command!("list-accounts"),
            clap::command!("add-account").args(&[
                clap::arg!(<address> "address of the server"),
                clap::arg!(<username> "account username"),
                clap::arg!(<password> "account password"),
            ]),
            clap::command!("create-account").args(&[
                clap::arg!(<address> "address of the server"),
                clap::arg!(<username> "account username"),
                clap::arg!(<password> "account password"),
            ]),
            clap::command!("forget-account").args(&[
                clap::arg!(<address> "address of the server"),
                clap::arg!(<username> "account username"),
            ]),
        ]);

    let matches = cmd.get_matches();
    match matches.subcommand() {
        Some(("list-accounts", _matches)) => {
            for account in config.accounts.iter() {
                println!(
                    "{} on {}",
                    account.username.as_ref().unwrap().yellow(),
                    account.server_address.as_ref().unwrap().yellow()
                );
            }
        }
        Some(("list-posts", matches)) => {
            let address = matches.get_one::<String>("address").unwrap();

            #[derive(Deserialize)]
            struct User {
                username: String,
            }

            #[derive(Deserialize)]
            struct PostComment {
                title: String,
                created: String,
                creator: User,
            }

            #[derive(Deserialize)]
            struct Post {
                id: u64,
                title: String,
                imgurl: String,
                created: String,
                creator: User,
                comments: Vec<PostComment>,
            }

            let posts: Vec<Post> = http_client
                .get(format!("{address}/api/frontpage"))
                .send()
                .unwrap()
                .json()
                .unwrap();

            for post in posts {
                println!(
                    "Post {} by {} posted at {}: {}",
                    post.id,
                    post.creator.username.green(),
                    format_time(post.created.as_str()),
                    post.title.yellow()
                );

                let image_url = format!("{address}{}", post.imgurl);
                let image = (|| -> Result<DynamicImage> {
                    let image_bytes = http_client.get(image_url.as_str()).send()?.bytes()?;
                    Ok(Reader::new(Cursor::new(image_bytes))
                        .with_guessed_format()?
                        .decode()?)
                })();
                match image {
                    Ok(image) => {
                        viuer::print(
                            &image,
                            &viuer::Config {
                                absolute_offset: false,
                                x: 2,
                                y: 2,
                                height: Some(30),
                                ..Default::default()
                            },
                        )
                        .unwrap();
                    }
                    Err(err) => {
                        println!("{error_text}: Couldn't load image: {err}]");
                    }
                }

                println!("Image URL: {}", image_url);
                println!("Comments ({}):", post.comments.len());
                for comment in post.comments {
                    println!(
                        "\t{} at {}: {}",
                        comment.creator.username.green(),
                        format_time(comment.created.as_str()),
                        comment.title.yellow()
                    );
                }
                println!();
            }
        }
        Some(("create-post", matches)) => {
            let address = matches.get_one::<String>("address").unwrap();
            let username = matches.get_one::<String>("username").unwrap();
            let title = matches.get_one::<String>("title").unwrap();
            let image_file = matches.get_one::<String>("image_file").unwrap();

            let Some(account) = config.accounts.iter().find(|account| account.server_address.as_ref().unwrap() == address && account.username.as_ref().unwrap() == username) else {
                eprintln!("{error_text}: account does not exist");
                std::process::exit(1);
            };

            http_client
                .post(format!("{address}/post"))
                .header(
                    reqwest::header::COOKIE,
                    make_token_cookie(account.session_token.as_ref().unwrap().as_str()),
                )
                .multipart(
                    reqwest::blocking::multipart::Form::new()
                        .file("file", image_file.clone())
                        .unwrap()
                        .text("title", title.clone()),
                )
                .send()
                .unwrap()
                .error_for_status()
                .unwrap();
        }
        Some(("delete-post", matches)) => {
            let address = matches.get_one::<String>("address").unwrap();
            let username = matches.get_one::<String>("username").unwrap();
            let post = matches.get_one::<String>("post").unwrap();

            let Some(account) = config.accounts.iter().find(|account| account.server_address.as_ref().unwrap() == address && account.username.as_ref().unwrap() == username) else {
                eprintln!("{error_text}: account does not exist");
                std::process::exit(1);
            };

            #[derive(Serialize)]
            struct DeletePostRequest {
                id: String,
            }

            http_client
                .post(format!("{address}/api/delete"))
                .header(
                    reqwest::header::COOKIE,
                    make_token_cookie(account.session_token.as_ref().unwrap().as_str()),
                )
                .json(&DeletePostRequest { id: post.clone() })
                .send()
                .unwrap()
                .error_for_status()
                .unwrap();
        }
        Some(("create-comment", matches)) => {
            let address = matches.get_one::<String>("address").unwrap();
            let username = matches.get_one::<String>("username").unwrap();
            let post = matches.get_one::<String>("post").unwrap();
            let text = matches.get_one::<String>("text").unwrap();

            let Some(account) = config.accounts.iter().find(|account| account.server_address.as_ref().unwrap() == address && account.username.as_ref().unwrap() == username) else {
                eprintln!("{error_text}: account does not exist");
                std::process::exit(1);
            };

            #[derive(Serialize)]
            struct CreateCommentRequest {
                id: String,
                title: String,
            }

            http_client
                .post(format!("{address}/api/comment"))
                .header(
                    reqwest::header::COOKIE,
                    make_token_cookie(account.session_token.as_ref().unwrap().as_str()),
                )
                .json(&CreateCommentRequest {
                    id: post.clone(),
                    title: text.clone(),
                })
                .send()
                .unwrap()
                .error_for_status()
                .unwrap();
        }
        Some(("add-account", matches)) => {
            let address = matches.get_one::<String>("address").unwrap();
            let username = matches.get_one::<String>("username").unwrap();
            let password = matches.get_one::<String>("password").unwrap();

            let already_exists = config
                .accounts
                .iter()
                .any(|account| {
                    account.server_address.as_ref().unwrap() == address
                        && account.username.as_ref().unwrap() == username
                });

            if already_exists {
                eprintln!("{error_text}: account has already been added");
                std::process::exit(1);
            }

            #[derive(Serialize)]
            struct LoginRequest {
                username: String,
                password: String,
            }
            #[derive(Deserialize)]
            struct LoginResponse {
                key: String,
            }
            let login_http_response = match http_client
                .post(format!("{address}/api/login"))
                .json(&LoginRequest {
                    username: username.clone(),
                    password: password.clone(),
                })
                .send()
            {
                Ok(response) => response,
                Err(err) => {
                    eprintln!("{error_text}: login failed: {}", err);
                    std::process::exit(1);
                }
            };

            let login_response: LoginResponse = login_http_response.json().unwrap();

            config.accounts.push(data::Account {
                server_address: Some(address.clone()),
                username: Some(username.clone()),
                session_token: Some(login_response.key),
            })
        }
        Some(("create-account", matches)) => {
            let address = matches.get_one::<String>("address").unwrap();
            let username = matches.get_one::<String>("username").unwrap();
            let password = matches.get_one::<String>("password").unwrap();

            let already_exists = config
                .accounts
                .iter()
                .any(|account| {
                    account.server_address.as_ref().unwrap() == address
                        && account.username.as_ref().unwrap() == username
                });

            if already_exists {
                eprintln!("{error_text}: account has already been added");
                std::process::exit(1);
            }

            http_client
                .post(format!("{address}/signup"))
                .multipart(
                    reqwest::blocking::multipart::Form::new()
                        .text("username", username.clone())
                        .text("password", password.clone()),
                )
                .send()
                .unwrap()
                .error_for_status()
                .unwrap();
        }
        Some(("forget-account", matches)) => {
            let address = matches.get_one::<String>("address").unwrap();
            let username = matches.get_one::<String>("username").unwrap();
            let account_index = config.accounts.iter().position(|account| {
                account.server_address.as_ref().unwrap() == address
                    && account.username.as_ref().unwrap() == username
            });

            match account_index {
                Some(account_index) => {
                    config.accounts.remove(account_index);
                }
                None => {
                    eprintln!("{error_text}: account does not exist");
                    std::process::exit(1);
                }
            }
        }
        _ => unreachable!("clap should ensure we don't get here"),
    };

    // Truncate the file
    config_file_lock.file.set_len(0).unwrap();
    config_file_lock.file.seek(SeekFrom::Start(0)).unwrap();

    // Save the config
    config_file_lock
        .file
        .write_all(config.encode_to_vec().as_slice())
        .unwrap();
    config_file_lock.file.sync_all().unwrap();
}
